package ua.com.parking.service;

import ua.com.parking.model.ReservationModel;
import ua.com.parking.model.SlotModel;

import java.time.Instant;
import java.util.List;

public interface BookingService {

    List<SlotModel> findFreeSlots(Instant from, Instant to);

    void reserve(String slotID, Instant timeForBooking);

    void cancel(ReservationModel reservation);
}
